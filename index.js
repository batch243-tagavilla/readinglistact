const http = require("http");
const port = 8000;

http.createServer((request, response) => {
    if ((request.url == "/") || (request.url == "/home")) {
        response.writeHead(200, {"Content-Type":"text/plain"});
        response.end(`This is the Homepage`);
    } else if ((request.url == "/login")) {
        response.writeHead(200, {"Content-Type":"text/plain"});
        response.end(`This is the Login page`);
    } else if ((request.url == "/register")) {
        response.writeHead(200, {"Content-Type":"text/plain"});
        response.end(`This is the Registration page`);
    } else if ((request.url == "/userProfile")) {
        response.writeHead(200, {"Content-Type":"text/plain"});
        response.end(`This is the User Profile page`);
    } else {
        response.writeHead(400, {"Content-Type":"text/plain"});
        response.end(`Page not found`);
    }
}).listen(port);

console.log(`Server running at localhost:${port}`);