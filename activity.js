/* 1. */
db.fruits.aggregate([
    {
        $match: {
            price: {$gte: 20}
        }
    }
]).pretty();

/* 2. */
db.fruits.aggregate([
    {
        $match: {
            $and: [
                {stock: {$gt: 10}},
                {stock: {$lt: 30}}
            ]
        }
    }
]).pretty();

/* 3. */
db.fruits.find({
    $and: [
        {
            name: {
                $regex: "a"
            }
        },
        {
            supplier_id: 1
        }
    ]
}).pretty();

/* 4. */
db.fruits.updateMany(
    {
        name: {
            $regex: "n"
        }
    },
    {
        $set: {
            supplier_id: 1
        }
    }
);

/* 5. */
db.fruits.updateMany(
    {
        stock: {
            $lte: 15
        }
    },
    {
        $set: {
            price: 200
        }
    }
);